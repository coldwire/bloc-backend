package utils

type Reponse struct {
	Code    int         `json:"-"`
	Status  string      `json:"status"`
	Message string      `json:"message"`
	Content interface{} `json:"content"`
}
