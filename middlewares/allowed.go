package middlewares

import (
	"bloc/utils/config"
	errors "bloc/utils/errs"

	"github.com/gofiber/fiber/v2"
)

func IsOauthEnabled(c *fiber.Ctx) error {
	if config.Conf.Oauth.Server != "" {
		return errors.Handle(c, errors.ErrOAuthEnable)
	}

	return c.Next()
}
