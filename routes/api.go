package routes

import (
	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
)

func Api(app *fiber.App) {
	api := app.Group("/")

	log.Debug().Msg("Loading auth routes")
	setupAuthRoutes(api)

	log.Debug().Msg("Loading users routes")
	setupUsersRoutes(api)

	log.Debug().Msg("Loading blocs routes")
	setupBlocsRoutes(api)
}
