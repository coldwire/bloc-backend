package routes

import (
	"bloc/controller/blocs"
	"bloc/middlewares"

	"github.com/gofiber/fiber/v2"
)

func setupBlocsRoutes(api fiber.Router) {
	// Auth > /api/auth
	blocsRoute := api.Group("/blocs", middlewares.IsAuthenticated)
	blocsRoute.Post("/upload", blocs.Upload)
	blocsRoute.Post("/create", blocs.Create)
	blocsRoute.Put("/move/:id/:new", blocs.Move)
	blocsRoute.Get("/download/:id", blocs.Download)
	blocsRoute.Get("/list/:id", blocs.List)
	blocsRoute.Delete("/delete/:id", blocs.Delete)
}
