{
    "swagger": "2.0",
    "info": {
        "description": "Store, and share file anonymously, using a resilient decentralized network with modern encryption.",
        "title": "Coldwire Bloc API",
        "contact": {},
        "license": {
            "name": "NPOSL-3.0",
            "url": "https://opensource.org/licenses/NPOSL-3.0"
        },
        "version": "beta 4"
    },
    "paths": {
        "/auth/login": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Auth"
                ],
                "summary": "Create a new session for a user",
                "parameters": [
                    {
                        "description": "Login request",
                        "name": "Content",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/auth.LoginRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/auth.LoginResponse"
                        }
                    }
                }
            }
        },
        "/auth/logout": {
            "get": {
                "tags": [
                    "Auth"
                ],
                "summary": "Terminate user's session",
                "responses": {}
            }
        },
        "/auth/register": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Auth"
                ],
                "summary": "Create a new user account with username and password",
                "parameters": [
                    {
                        "description": "register",
                        "name": "Content",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/auth.RegisterRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/auth.RegisterResponse"
                        }
                    }
                }
            }
        },
        "/blocs/create": {
            "post": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Blocs"
                ],
                "summary": "Create a new bloc folder",
                "parameters": [
                    {
                        "description": "Create request",
                        "name": "Content",
                        "in": "body",
                        "required": true,
                        "schema": {
                            "$ref": "#/definitions/blocs.CreateRequest"
                        }
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/blocs.CreateResponse"
                        }
                    }
                }
            }
        },
        "/blocs/delete/{id}": {
            "delete": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Blocs"
                ],
                "summary": "Delete bloc from the server (folder or file)",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Id of the bloc to delete",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        },
        "/blocs/download/{id}": {
            "get": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "multipart/form-data"
                ],
                "tags": [
                    "Blocs"
                ],
                "summary": "Download a file",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Id of the bloc to download",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        },
        "/blocs/list/{id}": {
            "get": {
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Blocs"
                ],
                "summary": "List blocs of a parent bloc",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Parent bloc",
                        "name": "id",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/blocs.ListResponse"
                        }
                    }
                }
            }
        },
        "/blocs/move/{id}/{new}": {
            "put": {
                "consumes": [
                    "application/json"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Blocs"
                ],
                "summary": "Change parent of a bloc",
                "parameters": [
                    {
                        "type": "string",
                        "description": "Id of the bloc to move",
                        "name": "id",
                        "in": "path",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "Id of the new parent",
                        "name": "new",
                        "in": "path",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK"
                    }
                }
            }
        },
        "/blocs/upload": {
            "post": {
                "description": "Upload a new file and store it as a bloc",
                "consumes": [
                    "multipart/form-data"
                ],
                "produces": [
                    "application/json"
                ],
                "tags": [
                    "Blocs"
                ],
                "summary": "Upload",
                "parameters": [
                    {
                        "type": "file",
                        "description": "File to upload",
                        "name": "file",
                        "in": "formData",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "Encrypted encryption key of the file",
                        "name": "key",
                        "in": "formData",
                        "required": true
                    },
                    {
                        "type": "string",
                        "description": "Parent bloc of this bloc",
                        "name": "parent",
                        "in": "formData",
                        "required": true
                    }
                ],
                "responses": {
                    "200": {
                        "description": "OK",
                        "schema": {
                            "$ref": "#/definitions/blocs.UploadResponse"
                        }
                    }
                }
            }
        }
    },
    "definitions": {
        "auth.LoginRequest": {
            "type": "object",
            "properties": {
                "password": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "auth.LoginResponse": {
            "type": "object",
            "properties": {
                "private_key": {
                    "type": "string"
                },
                "public_key": {
                    "type": "string"
                },
                "root": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "auth.RegisterRequest": {
            "type": "object",
            "properties": {
                "password": {
                    "type": "string"
                },
                "private_key": {
                    "type": "string"
                },
                "public_key": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "auth.RegisterResponse": {
            "type": "object",
            "properties": {
                "private_key": {
                    "type": "string"
                },
                "public_key": {
                    "type": "string"
                },
                "root": {
                    "type": "string"
                },
                "username": {
                    "type": "string"
                }
            }
        },
        "blocs.CreateRequest": {
            "type": "object",
            "properties": {
                "name": {
                    "type": "string"
                },
                "parent": {
                    "type": "string"
                }
            }
        },
        "blocs.CreateResponse": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "owner": {
                    "type": "string"
                },
                "parent": {
                    "type": "string"
                }
            }
        },
        "blocs.ListResponse": {
            "type": "object",
            "properties": {
                "bloc_cursor": {
                    "type": "string"
                },
                "current_bloc_list": {
                    "type": "array",
                    "items": {
                        "type": "string"
                    }
                },
                "current_bloc_map": {
                    "type": "object",
                    "additionalProperties": true
                }
            }
        },
        "blocs.UploadResponse": {
            "type": "object",
            "properties": {
                "id": {
                    "type": "string"
                },
                "is_favorite": {
                    "type": "boolean"
                },
                "mime_type": {
                    "type": "string"
                },
                "name": {
                    "type": "string"
                },
                "owner": {
                    "type": "string"
                },
                "parent": {
                    "type": "string"
                },
                "size": {
                    "type": "integer"
                }
            }
        }
    }
}