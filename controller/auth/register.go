package auth

import (
	"bloc/models"
	"bloc/utils"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"
	"log"
	"regexp"
	"time"

	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

type RegisterRequest struct {
	Username   string `json:"username"`
	Password   string `json:"password"`
	PrivateKey string `json:"private_key"`
	PublicKey  string `json:"public_key"`
}

type RegisterResponse struct {
	Username   string `json:"username"`
	PublicKey  string `json:"public_key"`
	PrivateKey string `json:"private_key"`
	Root       string `json:"root"`
}

// Register godoc
// @Summary      Create a new user account with username and password
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        Content  body  RegisterRequest  true  "register"
// @Success      200  {object}  RegisterResponse
// @Router       /auth/register [POST]
func Register(c *fiber.Ctx) error {
	var request = RegisterRequest{}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody, err)
	}

	var root = models.Bloc{
		Id:       uuid.NewString(),
		Name:     "root",
		BlocType: "FOLDER",
	}

	var usr = models.User{
		Username:   request.Username,
		PrivateKey: request.PrivateKey,
		PublicKey:  request.PublicKey,
		AuthMode:   "LOCAL",
	}

	usernameValidation, err := regexp.MatchString("[a-zA-Z]{3,}", request.Username)
	if !usernameValidation {
		return errors.Handle(c, errors.ErrBody, err)
	}

	if len(request.Password) <= 8 {
		return errors.Handle(c, errors.ErrBody, "password too short or invalid")
	}

	if len(request.PrivateKey) < 32 || len(request.PublicKey) < 44 {
		return errors.Handle(c, errors.ErrAuth, "keys are long enought/are not keys")
	}

	exist, err := usr.Exist()
	if !exist && err != nil {
		log.Print("aaa")
		return errors.Handle(c, errors.ErrAuth, err)
	}

	if exist {
		return errors.Handle(c, errors.ErrAuthExist)
	}

	hash, err := argon2id.CreateHash(request.Password, argon2id.DefaultParams)
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	usr.Password = hash

	// Create root folder
	err = root.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	usr.Root = root.Id // add root folder to the user for the response

	err = usr.Create()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = usr.SetRoot(root.Id) // Set root folder of the user
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	err = root.SetOwner(usr.Username) // Set the owner of the root folder
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseCreate, err)
	}

	token := tokens.Generate(tokens.Token{
		Username: request.Username,
	}, 12*time.Hour)

	utils.SetCookie(c, "token", token, time.Now().Add(time.Hour*6))

	return errors.Handle(c, errors.Success, RegisterResponse{
		Username:   usr.Username,
		PublicKey:  usr.PublicKey,
		PrivateKey: usr.PrivateKey,
		Root:       usr.Root,
	})
}
