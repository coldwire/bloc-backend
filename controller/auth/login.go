package auth

import (
	"bloc/models"
	"bloc/utils"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"
	"regexp"
	"time"

	"github.com/alexedwards/argon2id"
	"github.com/gofiber/fiber/v2"
)

type LoginRequest struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	Username   string `json:"username"`
	PublicKey  string `json:"public_key"`
	PrivateKey string `json:"private_key"`
	Root       string `json:"root"`
}

// Login godoc
// @Summary      Create a new session for a user
// @Tags         Auth
// @Accept       json
// @Produce      json
// @Param        Content  body  LoginRequest  true  "Login request"
// @Success      200  {object}  LoginResponse
// @Router       /auth/login [POST]
func Login(c *fiber.Ctx) error {
	var request = LoginRequest{}

	err := c.BodyParser(&request)
	if err != nil {
		return errors.Handle(c, errors.ErrBody)
	}

	var user = models.User{
		Username: request.Username,
	}

	usernameValidation, _ := regexp.MatchString("[a-zA-Z]{3,}", request.Username)
	if !usernameValidation {
		return errors.Handle(c, errors.ErrAuthInvalidUsername)
	}

	user, err = user.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	isValid, err := argon2id.ComparePasswordAndHash(request.Password, user.Password)
	if !isValid {
		return errors.Handle(c, errors.ErrAuthWrongPassword)
	}

	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	token := tokens.Generate(tokens.Token{
		Username: request.Username,
	}, 12*time.Hour)

	utils.SetCookie(c, "token", token, time.Now().Add(time.Hour*6))

	return errors.Handle(c, errors.Success, LoginResponse{
		Username:   user.Username,
		PublicKey:  user.PublicKey,
		PrivateKey: user.PrivateKey,
		Root:       user.Root,
	})
}
