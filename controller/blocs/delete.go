package blocs

import (
	"bloc/models"
	"bloc/storage"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"

	"github.com/gofiber/fiber/v2"
	"github.com/rs/zerolog/log"
)

func deleteFile(b models.Bloc) error {
	err := storage.Driver.Delete(b.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	properties := models.BlocProperties{
		Id: b.BlocProperty,
	}

	err = properties.Delete()
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return nil
}

func deleteCascade(b models.Bloc) error {
	blocs := models.Bloc{
		Parent: b.Id,
	}

	blocList, err := blocs.List()
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	for _, bl := range blocList {
		if bl.BlocType == "FILE" {
			deleteFile(bl)
		} else {
			deleteCascade(models.Bloc{
				Id: bl.Id,
			})
		}
	}

	return nil
}

// Delete godoc
// @Summary      Delete bloc from the server (folder or file)
// @Tags         Blocs
// @Accept       json
// @Produce      json
// @Param        id  path  string  true  "Id of the bloc to delete"
// @Success      200
// @Router       /blocs/delete/{id} [DELETE]
func Delete(c *fiber.Ctx) error {
	id := c.Params("id")

	bloc := models.Bloc{
		Id: id,
	}

	token, err := tokens.Parse(c.Cookies("token"))
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	b, err := bloc.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	if b.Owner != token.Username {
		return errors.Handle(c, errors.ErrPermission)
	}

	// Delete from database
	err = b.Delete()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseRemove, err)
	}

	// If the bloc is a file, delete from the storage
	// If it's a folder with start a loop to scan all the subfolder
	// and files to delete them
	if b.BlocType == "FILE" {
		err := deleteFile(b)
		if err != nil {
			return errors.Handle(c, errors.ErrUnknown, err)
		}
	} else {
		err := deleteCascade(b)
		if err != nil {
			return errors.Handle(c, errors.ErrUnknown, err)
		}
	}

	return errors.Handle(c, errors.Success)
}
