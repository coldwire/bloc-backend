package blocs

import (
	"bloc/models"
	errors "bloc/utils/errs"
	"bloc/utils/tokens"
	"time"

	"github.com/gofiber/fiber/v2"
)

type ListResponse struct {
	BlocCursor      string                 `json:"bloc_cursor"`
	CurrentBlocList []string               `json:"current_bloc_list"`
	CurrentBlocMap  map[string]interface{} `json:"current_bloc_map"`
}

type ListResponseFolder struct {
	Id          string    `json:"id"`
	Name        string    `json:"name"`
	IsDirectory bool      `json:"is_directory"`
	Parent      string    `json:"parent"`
	CreatedOn   time.Time `json:"created_on"`
	EditedOn    time.Time `json:"edited_on"`
}

type ListResponseFile struct {
	Id          string    `json:"id"`
	Name        string    `json:"name"`
	IsDirectory bool      `json:"is_directory"`
	Parent      string    `json:"parent"`
	Size        int       `json:"size"`
	MimeType    string    `json:"mime_type"`
	IsFavorite  bool      `json:"is_favorite"`
	CreatedOn   time.Time `json:"created_on"`
	EditedOn    time.Time `json:"edited_on"`
}

// List godoc
// @Summary      List blocs of a parent bloc
// @Tags         Blocs
// @Produce      json
// @Param        id  path  string  true  "Parent bloc"
// @Success      200   		   {object}  ListResponse
// @Router       /blocs/list/{id} [GET]
func List(c *fiber.Ctx) error {
	parent := c.Params("id") // Id of the parent
	if parent == "" {
		return errors.Handle(c, errors.ErrRequest, "please give a parent bloc ID")
	}

	token, err := tokens.Parse(c.Cookies("token")) // Parse user's JWT token
	if err != nil {
		return errors.Handle(c, errors.ErrAuth, err)
	}

	bloc := models.Bloc{
		Id: parent,
	}

	bloc, err = bloc.Find()
	if err != nil {
		return errors.Handle(c, errors.ErrDatabaseNotFound, err)
	}

	if bloc.Owner != token.Username {
		return errors.Handle(c, errors.ErrPermission, err)
	}

	childBlocs := models.Bloc{
		Parent: parent,
	}

	blocs, err := childBlocs.List()
	if err != nil {
		return errors.Handle(c, errors.ErrUnknown, err)
	}

	// Prepare response

	var res = ListResponse{
		BlocCursor:      parent,
		CurrentBlocList: []string{},
		CurrentBlocMap:  make(map[string]interface{}),
	}

	for _, b := range blocs {
		res.CurrentBlocList = append(res.CurrentBlocList, b.Id)

		if b.BlocType == "FILE" {
			properties := models.BlocProperties{Id: b.BlocProperty}
			properties, err = properties.Find()
			if err != nil {
				return errors.Handle(c, errors.ErrDatabaseGeneric, err)
			}

			res.CurrentBlocMap[b.Id] = ListResponseFile{
				Id:          b.Id,
				Name:        b.Name,
				IsDirectory: false,
				Parent:      parent,
				Size:        properties.Size,
				MimeType:    properties.MimeType,
				IsFavorite:  properties.IsFavorite,
				CreatedOn:   b.CreatedOn,
				EditedOn:    b.EditedOn,
			}
		} else {
			res.CurrentBlocMap[b.Id] = ListResponseFolder{
				Id:          b.Id,
				Name:        b.Name,
				IsDirectory: true,
				Parent:      parent,
				CreatedOn:   b.CreatedOn,
				EditedOn:    b.EditedOn,
			}
		}
	}

	return errors.Handle(c, errors.Success, res)
}
