package main

import (
	"bloc/database"
	"bloc/routes"
	"bloc/storage"
	"bloc/utils/config"
	"bloc/utils/env"
	"bloc/utils/tokens"
	"flag"
	"os"

	"codeberg.org/coldwire/cwauth"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/gofiber/swagger"
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"golang.org/x/oauth2"

	_ "bloc/docs"
)

func init() {
	// Configure logs
	log.Logger = log.Output(zerolog.ConsoleWriter{Out: os.Stderr})

	fileConf := flag.String("config", "", "Path to the config file")
	flag.Parse()

	// Init configuration
	config.Init(env.Get("CONFIG_FILE", *fileConf))

	// Connect to database
	database.Connect()

	// Init oauth client
	if config.Conf.Oauth.Server != "" {
		cwauth.InitOauth2(oauth2.Config{
			ClientID:     config.Conf.Oauth.Id,
			ClientSecret: config.Conf.Oauth.Secret,
			RedirectURL:  config.Conf.Oauth.Callback,
		}, config.Conf.Oauth.Server)
	}

	// Init storage backend
	storage.Init(config.Conf.Storage.Driver)

	// Generate JWT token
	tokens.Init(env.Get("JWT_KEY", ""))
}

// @title Coldwire Bloc API
// @version beta 4
// @description Store, and share file anonymously, using a resilient decentralized network with modern encryption.
// @license.name NPOSL-3.0
// @license.url https://opensource.org/licenses/NPOSL-3.0
func main() {
	// Create fiber instance
	app := fiber.New(fiber.Config{
		BodyLimit: (1024 * 1024 * 1024) * 8, // Limit file upload size (8Gb)
	})

	// Include cors
	app.Use(cors.New())

	// Swagger api documentation
	app.Get("/doc/*", swagger.New(swagger.Config{
		Layout:       "BaseLayout",
		DeepLinking:  false,
		DocExpansion: "list",
	}))

	// Setup API routes
	routes.Api(app)

	log.Info().Err(app.Listen(config.Conf.Server.Address + ":" + config.Conf.Server.Port))
}
