package models

import (
	"bloc/database"
	"context"

	"github.com/georgysavva/scany/pgxscan"
	"github.com/rs/zerolog/log"
)

type BlocProperties struct {
	Id         string `db:"id"          json:"id"`
	Size       int    `db:"size"        json:"size"`
	MimeType   string `db:"mime_type"   json:"mime_type"`
	IsFavorite bool   `db:"is_favorite" json:"is_favorite"`
	Key        string `db:"key"         json:"key"`
}

func (bp BlocProperties) Create() error {
	_, err := database.DB.Exec(context.Background(), `
	INSERT INTO bloc_properties (
		id,
		size,
		mime_type,
		is_favorite,
		key
	) VALUES($1, $2, $3, $4, $5)`, bp.Id, bp.Size, bp.MimeType, bp.IsFavorite, bp.Key)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return nil
}

func (bp BlocProperties) Delete() error {
	_, err := database.DB.Exec(context.Background(), `DELETE FROM bloc_properties WHERE id = $1`, bp.Id)
	if err != nil {
		log.Err(err).Msg(err.Error())
		return err
	}

	return err
}

func (bp BlocProperties) Find() (BlocProperties, error) {
	var properties BlocProperties
	err := pgxscan.Get(context.Background(), database.DB, &properties, `
	SELECT
		id,
		size,
		mime_type,
		is_favorite,
		key
			FROM bloc_properties
				WHERE id = $1`, bp.Id)

	if err != nil {
		log.Err(err).Msg(err.Error())
		return properties, err
	}

	return properties, nil
}
